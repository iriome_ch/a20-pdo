<?php
include "../private/pdo.php";
if (isset($_GET["name"])) {
    $username = $_GET["name"];
} else {
    die("Falta el nombre del parametro");
    //header("Location: index.php");
}
$sql = "SELECT * from autos";
$smt = $pdo->prepare($sql);
$smt->execute();
if ((isset($_POST['submit']) && (is_numeric($_POST['v-year'])) && (is_numeric($_POST['kms'])) && (!empty($_POST['brand'])))) {
    $brand = $_POST['brand'];
    $year = $_POST['v-year'];
    $km = $_POST['kms'];
    $i_sql = "INSERT INTO autos (make,year,mileage) VALUES ('$brand','$year' ,'$km' )";
    $pdo->exec($i_sql) ?>
    <div class="card">
        <div class="card-body text-success text-center">
            Registro insertado
        </div>
    </div>
<?php
} elseif ((isset($_POST['submit']) && !(is_numeric($_POST['v-year'])) && !(is_numeric($_POST['kms']) && (!empty($_POST['brand']))))) { ?>
    <div class="card">
        <div class="card-body text-danger text-center">
            Kilometraje y año deben ser numéricos.
        </div>
    </div>
<?php
} elseif (empty($_POST['brand'])) {
?>
    <div class="card">
        <div class="card-body text-danger text-center">
            El campo marca es obligatorio.
        </div>
    </div>
<?php
}
if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $d_sql = "DELETE FROM autos where audio_id=$id";
    $pdo->exec($d_sql); ?>
    <div class="card">
        <div class="card-body text-danger text-center">
            Registro borrado
        </div>
    </div>
<?php
}
if (isset($_POST['logout'])) {
    header('Location: index.php') and die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row align-items-center">
            <div class="col">
                <form action="" method="post">
                    <div class=" mb-3">
                        <label for="brand" class="form-label">Brand</label>
                        <input type="text" name="brand" id="brand" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="kms" class="form-label">Kilometres</label>
                        <input type="text" name="kms" id="kms" class="form-control">
                    </div>
                    <div class=" mb-3">
                        <label for="v-year" class="form-label">Car Years</label>
                        <input type="text" name="v-year" id="v-year" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-outline-success" name="submit">Insert Car</button>
                </form>
            </div>
            <div class="col">
                <table class="table <?php ?>">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Brand</th>
                            <th>Kilometres</th>
                            <th>Years</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $rows = $smt->fetchAll(PDO::FETCH_OBJ);
                        foreach ($rows as $row) {
                        ?>
                            <tr>
                                <th><?= $row->audio_id; ?></th>
                                <td><?= $row->make; ?></td>
                                <td><?= $row->mileage; ?></td>
                                <td><?= $row->year; ?></td>
                                <td>
                                    <form action="" method="post">

                                        <input type="hidden" name="id" value="<?= $row->audio_id; ?>">
                                        <input type="submit" class="btn btn-outline-danger" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col">
                <button class="btn btn-outline-dark" name="logout">log out</button>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>