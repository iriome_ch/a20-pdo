<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Welcome to CRUD App</h1>
    <a href="login.php">Please log in</a>
    <p>Attempt to go to <a href="autos.php">autos.php</a> without logging in - it should fail with an error message.</p>
</body>

</html>